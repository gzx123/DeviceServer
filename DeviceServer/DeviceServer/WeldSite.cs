﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLCommon;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DeviceServer
{
    /// <summary>
    /// 焊接设备
    /// </summary>
   public class WeldSite:Site
    {



        #region 重写公共方法
        /// <summary>
        /// 解析内容
        /// </summary>
        /// <param name="content"></param>
        /// <param name="steps"></param>
        public async override void Parse(string content, List<string> steps)
        {
            try
            {
                var result = JsonConvert.DeserializeObject<ContentJson>(content);
               var item= result.submit.items.SingleOrDefault(i => i.name.Contains("总分"));
              
                //var score = result.submit.items.Sum(i => i.value);
                var score = item.value;
                if (score > 40)
                    score = 40;
                await Submit(score, "k2");
            }
            catch (Exception ex)
            {
                LogTool.UpServerLog(ex.StackTrace, EventType.Error);
            }
          
        }

        /// <summary>
        /// 获取试卷
        /// </summary>
        /// <returns></returns>
        public override string GetPaper()
        {
            string r ;
            if (!string.IsNullOrEmpty(km))
            {
                r = km.Substring(km.Length - 1, 1);
            }
            else
            {
                Random random = new Random();
                r = (random.Next(4) + 1).ToString();
            }
            ////多出1，其次3
            //r = "1";
            //Random random  = new Random();
            //var temp = random.Next(5);
            //if (temp == 4)
            //    r = "3";
            var jdata = JObject.FromObject(new
            {
                paper=r
            });
            return jdata.ToString();
        }

        public async override void Roll()
        {
            string cmd = "roll";
            var senddata = Encoding.UTF8.GetBytes(cmd);
            await Session.SendAsync(senddata);
        }
        #endregion

        class ContentJson
        {
            public SubmitJson submit { get; set; }
        }

        class SubmitJson
        {
            public string nr { get; set; }
            public List<NameValue> items { get; set; }
        }

        class NameValue
        {
            public string name { get; set; }
            public decimal value { get; set; }
        }
    }
}
