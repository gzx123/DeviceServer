﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cowboy.Sockets;
using GLCommon;
using Newtonsoft.Json.Linq;

namespace DeviceServer
{
    public class Site
    {
        public Site()
        {
                
        }
        /// <summary>
        /// 准考证Id
        /// </summary>
        public string TitleId { get; set; }
        /// <summary>
        /// 具体科目(k21-k25)
        /// </summary>
        public string km { get; set; }
        /// <summary>
        /// 连接设备的session
        /// </summary>
        public TcpSocketSaeaSession Session { get; set; }
        /// <summary>
        /// 设备标识
        /// </summary>
        public string Serail { get; set; }
        /// <summary>
        /// 版本
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 心跳
        /// </summary>
        public bool IsAlive { get; set; }
        /// <summary>
        /// IP 作为key
        /// </summary>
        public string IP { get; set; }

        public SiteType SiteType { get; set; }

       
        
        //public LowVoltage LowVoltage { get; set; }

        public string GetSiteType()
        {
            string t = String.Empty;
            switch (SiteType)
            {
                case SiteType.lv:
                    t = "低压";
                    break;
                case SiteType.weld:
                    t = "焊接";
                    break;
                case SiteType.meter:
                    t = "仪器仪表";
                    break;
            }
            return t;
        }

        /// <summary>
        /// 获取题目
        /// </summary>
        /// <returns></returns>
        public virtual string GetPaper()
        {
            return null;
        }



        /// <summary>
        /// 解析返回值
        /// </summary>
        /// <param name="content"></param>
        /// <param name="steps"></param>
        public virtual void Parse(string content,List<String> steps )
        {

        }

        /// <summary>
        /// 收卷
        /// </summary>
        public virtual void Roll()
        {
            
        }

        /// <summary>
        /// 上传成绩
        /// </summary>
        /// <param name="score">分值</param>
        /// <param name="subject">科目  k2,k11,k41</param>
        /// <returns></returns>
        protected async Task Submit(decimal score,string subject)
        {
            var o = JObject.FromObject(
               new
               {
                   id = TitleId,
                   km = subject,
                   q = km,
                   score
               });
            try
            {
                var r = await HttpTool.PostAsJsonAsync("upscore", o);
                var temp = JObject.Parse(r);
                var status = (int)temp["status"];
                string msg = String.Empty;
                switch (status)
                {
                    case 0:
                        msg = "上传成功，得分：" + score;
                        break;
                    case 1:
                        msg = "考生不存在";
                        break;
                    case 2:
                        msg = (string)temp["phrase"];
                        break;
                    case 3:
                        msg = "考生考试成绩已存在";
                        break;
                    case 4:
                        msg = "考生补考成绩已存在";
                        break;
                    case -3:
                        msg = "服务器内部错误，请联系管理员";
                        break;
                    default:
                        msg = temp["phrase"] != null ? (string)temp["phrase"] : "";
                        break;
                }
                LogTool.WriteInfoLog(GetSiteType() + "设备上传成绩:返回值" + status+",score:"+score);
                LogTool.UpServerLog(GetSiteType() + "设备上传成绩:" + msg, EventType.Information);
            }
            catch (Exception ex)
            {
                var innerException = GetInnerException(ex);
                LogTool.UpServerLog(innerException.Message, EventType.Error);
            }
        }

        protected Exception GetInnerException(Exception ex)
        {
            if (ex.InnerException != null)
            {
                return GetInnerException(ex.InnerException);
            }
            return ex;
        }

        /// <summary>
        /// 上传设备状态
        /// </summary>
        /// <param name="dstate"></param>
        /// <returns></returns>

        protected async Task UpDeviceState(DeviceState dstate)
        {
            var o = JObject.FromObject(
                new
                {
                    serial = this.Serail,
                    state = dstate
                });
            try
            {
                var r = await HttpTool.PostAsJsonAsync("upstate", o);
                var temp = JObject.Parse(r);
                var status = (int)temp["status"];
                string msg = String.Empty;
                switch (status)
                {
                    case 0:
                        msg = "上传发题状态成功";
                        break;
                    case 1:
                        msg = "上传发题状态失败：考台不存在";
                        break;
                    case -3:
                        msg = "服务器内部错误，请联系管理员";
                        break;
                    default:
                        msg = temp["phrase"] != null ? (string)temp["phrase"] : "";
                        break;
                }
                LogTool.UpServerLog(msg, EventType.Information);
            }
            catch (Exception ex)
            {
                var innerException = GetInnerException(ex);
                LogTool.UpServerLog(innerException.Message, EventType.Error);
            }
        }
    }
   
    /// <summary>
    /// 考试状态
    /// </summary>
    public enum DeviceState
    {
        unlink=0,
        /// <summary>
        /// 空闲
        /// </summary>
        free = 1,
        /// <summary>
        /// 考试中
        /// </summary>
        examing = 2,
        /// <summary>
        /// 未重置
        /// </summary>
        unreset = 3,



    }

    /// <summary>
    /// site类型
    /// </summary>
    public enum SiteType
    {
        lv,
        weld,
        meter
    }
}
