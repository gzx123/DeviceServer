unit practiceLV;

interface

uses concept, leastCommonUnit, indyHelperUnit,
	IdContext, IdBaseComponent, IdComponent,  System.Generics.Collections,  System.Classes,
  IdCustomTCPServer, IdTCPServer, SysUtils, Winapi.Windows, winsock2, System.RegularExpressions, TypInfo, System.Math;

{$SCOPEDENUMS ON}

const
	fault_point_count=8; //排故题数量
	wiring_count=3; //接线题数量

type

TDeviceType=(A, B);

TWireName=(stopbutton, Zhengled, KM1, KM2, fanled, dengpao, button1, button2,
	Zhenliuqi, dankai1, dankai2,	Zlqbutton, slot1, slot2, updn, updn1, updn2,
	gnd1, gnd2, zzbutton, fzbutton,	EEmeter, CTransformer, Fuse);

	TDevice=record
		serial: string;
		orderNr: Byte;  //顺序号
		devType: TDeviceType;
	end;

	TPaper=record
		faultPoint: array[0..fault_point_count-1] of Boolean; //排故题的八个故障点，置为true表示出那道题
		wiring: Byte; //接线题序号
		faultPointScore: array[0..fault_point_count-1] of Currency; //每个故障点的分值
		function ToString(): string;
	//	wiringScore: Currency; //当前接线题的分值
	end;

	//反映电路板的状态
	TAnswer=record
		switch2: Boolean; //接线部分的开关对错
		onoff, shunxu: Boolean; //排故的两个开关对错
		faultPoint: array[0..fault_point_count-1] of Boolean; //排故题八个故障点的连接状态。
		wiring: TArray<TPair<TWireName, Boolean>>; //接线

		/// <summary>
		/// 保险丝是否已熔断
		/// </summary>
		fuse: CBool;
	end;

	CPracticeSiteLV=class
		ip: string;
		port: Word;
		device: TDevice;
		paper: TPaper;
		answer: TAnswer;
		procedure clone(dest: CPracticeSiteLV);
	end;

	CSiteList=class(TObjectList<CPracticeSiteLV>)
	public
		function indexOfIp(ip: string): CPracticeSiteLV;
	end;

	TFPDef=array[0..fault_point_count-1] of TByteSet;

	const
		FPDefA_CASE1: TFPDef=([1,4], [5, 10], [17, 29], [26,46], [27, 42], [12, 52], [53, 65], [33, 45]);
		FPDefB_CASE1: TFPDef=([1,3], [2,4], [9, 13], [5,16], [7, 8], [21, 22], [27, 29], [28, 30]);

		FPDefA_CASE2: TFPDef=([26,46], [27,42], [12,52], [53, 65], [63,17], [59,33], [5,10], [17,29]);
		FPDefB_CASE2: TFPDef=([1,3], [2,4], [9, 13], [5,16], [29,34], [30,39], [3,5], [4,6]);

		FPDefA_CASE3: TFPDef=([26,46], [27,42], [12,52], [53,65], [40,17], [24,33], [5,10], [17,29]); //17 63 40
		FPDefB_CASE3: TFPDef=([1,3], [2,4], [9, 13], [5,16], [29,34], [30,39], [3,5], [4,6]);

		FPDefA_CASE5: TFPDef=([26,46], [44, 48], [12,52], [53,65], [40,17], [24,33], [5,10], [17,29]); //17 63 40
		FPDefB_CASE5: TFPDef=([1,3], [2,4], [9, 13], [6, 15], [29,34], [30,39], [100, 102], [101, 103]);

type
CExamLV=class
private
	version_: Integer;
	callInMainThread: CCallInMainThread;
	tcpServer: TIdTCPServer;
	sites_: CSiteList;
	fOnSiteConnect: TProc<CPracticeSiteLV>;
	fOnSiteDisconnect: TProc<CPracticeSiteLV>;
	fOnSubmitPaper: TProc<CPracticeSiteLV>;
	fOnNotReset: TProc<CPracticeSiteLV>;
//	fDevConnectionMap: TDictionary<CDevice, TIdContext>;
//	sendReady: TDictionary<string, string>;
	procedure TcpServerOnExecute(AContext: TIdContext);
	procedure TcpServerOnConnect(AContext: TIdContext);
	procedure TcpServerOnDisconnect(AContext: TIdContext);
	procedure parse(request: string; site: CPracticeSiteLV);
public
	class var FPDefA, FPDefB: TFPDef;

	constructor Create();
	destructor Destroy; override;

	procedure setVersion(value: Integer);

	procedure open(port: Word); //throw

	property sites: CSiteList read sites_;

	property onSiteConnect: TProc<CPracticeSiteLV> read FonSiteConnect write FonSiteConnect;
	property onSiteDisconnect: TProc<CPracticeSiteLV> read FonSiteDisConnect write FonSiteDisConnect;

	procedure exam(site: CPracticeSiteLV);
	procedure roll(site: CPracticeSiteLV);
//	procedure test(dev: CDevice; content: string);

//	property onExam: TProc<CPracticeSite>
	property onNotReset: TProc<CPracticeSiteLV> read fOnNotReset write fOnNotReset;
	property onSubmitPaper: TProc<CPracticeSiteLV> read FonSubmitPaper write FonSubmitPaper;

	function calcScore(
		const site: CPracticeSiteLV;
		out faultPointScore: Currency;
		out wiringScore: Currency;
		out compose: CKeyValueCollection<string, Currency>): Currency;
	function randromPaper(devType: TDeviceType; info: string=''): TPaper;
end;

implementation

{ CServer }

type TWireItemScore=record
	wire: TWireName;
//	isAct: Boolean;
	score: Currency;
	constructor create(wire: TWireName; score: Currency);
end;

function CExamLV.calcScore(const site: CPracticeSiteLV; out faultPointScore, wiringScore: Currency; out compose: CKeyValueCollection<string, Currency>): Currency;

	function _getWireValue(wire: TWireName; check: Boolean=True): Boolean;
	var
		i: Integer;
	begin
	for i:=0 to High(site.answer.wiring) do
		if (site.answer.wiring[i].Key=wire) and (site.answer.wiring[i].Value=check) then begin
			if site.answer.wiring[i].Value then
				Exit(True)
			else
				Exit(False);
			end;
	Result:=False;
	end;

type
	TWireItemScores=array of  TWireItemScore;
	TQScores=array[0..2] of TWireItemScores;
	TScoring= array[TDeviceType] of TQScores;
var
	scoringVersion1: array[TDeviceType] of TQScores;
	scoringVersion2: array[TDeviceType] of TQScores;
	scoringVersion3: array[TDeviceType] of TQScores;
	scoringVersion4: array[TDeviceType] of TQScores;

	function _calcWireScore(const scoring: TWireItemScores): Currency;
	var
		i, j: Integer;
	begin
	Result:=0;
	for i:=0 to High(scoring) do
		for j:=0 to High(site.answer.wiring) do
			if (site.answer.wiring[j].Key= scoring[i].wire) and (site.answer.wiring[j].Value) then begin
				Result:=Result+scoring[i].score;
				compose.Add(CEnum.EnumToStr<twirename>(Ord(scoring[i].wire)), scoring[i].score);
				end;
	end;

var
	i: Integer;
begin
{$region 'version 1'}
scoringVersion1[TDeviceType.A][0]:=[
	TWireItemScore.create(TWireName.dengpao, 10),
	TWireItemScore.create(TWireName.button1, 10),
	TWireItemScore.create(TWireName.button2, 10)
	];
scoringVersion1[TDeviceType.A][1]:=[
	TWireItemScore.create(TWireName.Zhenliuqi, 15),
	TWireItemScore.create(TWireName.Zlqbutton, 15)
	];
scoringVersion1[TDeviceType.A][2]:=[
	TWireItemScore.create(TWireName.dankai2, 10),
	TWireItemScore.create(TWireName.dankai1, 10),
	TWireItemScore.create(TWireName.updn, 10)
	];
scoringVersion1[TDeviceType.B][0]:=[
	TWireItemScore.create(TWireName.KM1, 15),
	TWireItemScore.create(TWireName.Zhengled, 15)
	];
scoringVersion1[TDeviceType.B][1]:=[
	TWireItemScore.create(TWireName.stopbutton, 10),
	TWireItemScore.create(TWireName.Zhengled, 10),
	TWireItemScore.create(TWireName.KM1, 10)
	];
scoringVersion1[TDeviceType.B][2]:=[
	TWireItemScore.create(TWireName.KM2, 6),
	TWireItemScore.create(TWireName.fanled, 6),
	TWireItemScore.create(TWireName.stopbutton, 6),
	TWireItemScore.create(TWireName.Zhengled, 6),
	TWireItemScore.create(TWireName.KM1, 6)
	];
{$ENDREGION}

{$region 'version 3'}
scoringVersion3[TDeviceType.A][0]:=[
	TWireItemScore.create(TWireName.dengpao, 10),
	TWireItemScore.create(TWireName.button1, 10),
	TWireItemScore.create(TWireName.button2, 10)
	];
scoringVersion3[TDeviceType.A][1]:=[
	TWireItemScore.create(TWireName.Zhenliuqi, 15),
	TWireItemScore.create(TWireName.Zlqbutton, 15)
	];
scoringVersion3[TDeviceType.A][2]:=[
	TWireItemScore.create(TWireName.updn1, 5),
	TWireItemScore.create(TWireName.updn2, 5),
	TWireItemScore.create(TWireName.slot1, 5),
	TWireItemScore.create(TWireName.slot2, 5),
	TWireItemScore.create(TWireName.gnd1, 5),
	TWireItemScore.create(TWireName.gnd2, 5)
	];
scoringVersion3[TDeviceType.B][0]:=[
	TWireItemScore.create(TWireName.KM1, 7),
	TWireItemScore.create(TWireName.Zhengled, 7),
	TWireItemScore.create(TWireName.zzbutton, 8),
	TWireItemScore.create(TWireName.gnd1, 8)
	];
scoringVersion3[TDeviceType.B][1]:=[
	TWireItemScore.create(TWireName.stopbutton, 6),
	TWireItemScore.create(TWireName.Zhengled, 6),
	TWireItemScore.create(TWireName.KM1, 6),
	TWireItemScore.create(TWireName.gnd1, 6),
	TWireItemScore.create(TWireName.zzbutton, 6)
	];
scoringVersion3[TDeviceType.B][2]:=[
	TWireItemScore.create(TWireName.KM2, 3),
	TWireItemScore.create(TWireName.fanled, 3),
	TWireItemScore.create(TWireName.stopbutton, 4),
	TWireItemScore.create(TWireName.Zhengled, 4),
	TWireItemScore.create(TWireName.KM1, 4),
	TWireItemScore.create(TWireName.fzbutton, 4),
	TWireItemScore.create(TWireName.zzbutton, 4),
	TWireItemScore.create(TWireName.gnd1, 4)
	];
{$ENDREGION}

{$region 'version 4'}
scoringVersion4[TDeviceType.A][0]:=[
	TWireItemScore.create(TWireName.dengpao, 9),
	TWireItemScore.create(TWireName.button1, 9),
	TWireItemScore.create(TWireName.button2, 9),
	TWireItemScore.create(TWireName.EEmeter, 3)
	];
scoringVersion4[TDeviceType.A][1]:=[
	TWireItemScore.create(TWireName.Zhenliuqi, 13),
	TWireItemScore.create(TWireName.Zlqbutton, 14),
	TWireItemScore.create(TWireName.EEmeter, 3)
	];
scoringVersion4[TDeviceType.A][2]:=[
	TWireItemScore.create(TWireName.updn1, 5),
	TWireItemScore.create(TWireName.updn2, 4),
	TWireItemScore.create(TWireName.slot1, 5),
	TWireItemScore.create(TWireName.slot2, 4),
	TWireItemScore.create(TWireName.gnd1, 5),
	TWireItemScore.create(TWireName.gnd2, 4),
	TWireItemScore.create(TWireName.EEmeter, 3)
	];
scoringVersion4[TDeviceType.B][0]:=[
	TWireItemScore.create(TWireName.KM1, 7),
	TWireItemScore.create(TWireName.Zhengled, 6),
	TWireItemScore.create(TWireName.zzbutton, 7),
	TWireItemScore.create(TWireName.gnd1, 7),
	TWireItemScore.create(TWireName.CTransformer, 3)
	];
scoringVersion4[TDeviceType.B][1]:=[
	TWireItemScore.create(TWireName.stopbutton, 5),
	TWireItemScore.create(TWireName.Zhengled, 5),
	TWireItemScore.create(TWireName.KM1, 5),
	TWireItemScore.create(TWireName.gnd1, 6),
	TWireItemScore.create(TWireName.zzbutton, 6),
	TWireItemScore.create(TWireName.CTransformer, 3)
	];
scoringVersion4[TDeviceType.B][2]:=[
	TWireItemScore.create(TWireName.KM2, 3),
	TWireItemScore.create(TWireName.fanled, 3),
	TWireItemScore.create(TWireName.stopbutton, 3),
	TWireItemScore.create(TWireName.Zhengled, 3),
	TWireItemScore.create(TWireName.KM1, 3),
	TWireItemScore.create(TWireName.fzbutton, 4),
	TWireItemScore.create(TWireName.zzbutton, 4),
	TWireItemScore.create(TWireName.gnd1, 4),
	TWireItemScore.create(TWireName.CTransformer, 3)
	];
{$ENDREGION}

compose:=CKeyValueCollection<string, Currency>.Create();

//不考虑开关状态，计算排故分
faultPointScore:=0;
for i:=0 to High(site.paper.faultPoint) do begin
	if site.paper.faultPoint[i] then begin //如果该故障点是需要考生解答的
		if site.answer.faultPoint[i] then begin
			faultPointScore:=faultPointScore+site.paper.faultPointScore[i];
			compose.Add('故障点'+inttostr(i), site.paper.faultPointScore[i]);
			end;
		end
	else
		if site.answer.faultPoint[i] then begin //答了不该答的题
			faultPointScore:=faultPointScore + site.paper.faultPointScore[i]; //这里加的是负分值
			compose.Add('故障点'+inttostr(i), -site.paper.faultPointScore[i]);
			end;
	end;

//不考虑开关状态，计算接线分
wiringScore:=0;
case version_ of
	1: begin
		wiringScore:=_calcWireScore(scoringVersion1[site.device.devType][site.paper.wiring]);
		end;
	2: begin
		{$REGION ''}
		case site.device.devType of
			TDeviceType.A: begin
				case site.paper.wiring of
					0: begin
						if _getWireValue(TWireName.dengpao) then begin
							wiringScore:=wiringScore+10;
							compose.Add('dengpao', 10);
							end;
						if _getWireValue(TWireName.button1) then  begin
							wiringScore:=wiringScore+10;
							compose.Add('button1', 10);
							end;
						if _getWireValue(TWireName.button2) then begin
							wiringScore:=wiringScore+10;
							compose.Add('button2', 10);
							end;
						end;
					1: begin
						if _getWireValue(TWireName.Zhenliuqi) then begin
							wiringScore:=wiringScore+15;
							compose.Add('Zhenliuqi', 15);
							end;
						if _getWireValue(TWireName.Zlqbutton) then begin
							wiringScore:=wiringScore+15;
							compose.Add('Zlqbutton', 15);
							end;
						end;
					2: begin
						if _getWireValue(TWireName.dankai1) then begin
							wiringScore:=wiringScore+10;
							compose.Add('dankai1', 10);
							end;
						if _getWireValue(TWireName.dankai2) then begin
							wiringScore:=wiringScore+10;
							compose.Add('dankai2', 10);
							end;
						if _getWireValue(TWireName.updn) then begin
							wiringScore:=wiringScore+10;
							compose.Add('updn', 10);
							end;
						end;
					end;
				end;
			TDeviceType.B: begin
				case site.paper.wiring of
					0: begin
						if _getWireValue(TWireName.KM1) then begin
							wiringScore:=wiringScore+15;
							compose.Add('KM1', 15);
							end;
						if _getWireValue(TWireName.Zhengled) then begin
							wiringScore:=wiringScore+15;
							compose.Add('Zhengled', 15);
							end;
						end;
					1: begin
						if _getWireValue(TWireName.stopbutton) then begin
							wiringScore:=wiringScore+10;
							compose.Add('stopbutton', 10);
							end;
						if _getWireValue(TWireName.Zhengled) then begin
							wiringScore:=wiringScore+10;
							compose.Add('Zhengled', 10);
							end;
						if _getWireValue(TWireName.KM1) then begin
							wiringScore:=wiringScore+10;
							compose.Add('KM1', 10);
							end;
						end;
					2: begin
						if _getWireValue(TWireName.KM2) then begin
							wiringScore:=wiringScore+3;
							compose.Add('KM2', 3);
							end;
						if _getWireValue(TWireName.fanled) then begin
							wiringScore:=wiringScore+3;
							compose.Add('fanled', 3);
							end;
						if _getWireValue(TWireName.stopbutton) then begin
							wiringScore:=wiringScore+4;
							compose.Add('stopbutton', 4);
							end;
						if _getWireValue(TWireName.Zhengled) then begin
							wiringScore:=wiringScore+4;
							compose.Add('Zhengled', 4);
							end;
						if _getWireValue(TWireName.KM1) then begin
							wiringScore:=wiringScore+4;
							compose.Add('KM1', 4);
							end;
						if _getWireValue(TWireName.fzbutton) then begin
							wiringScore:=wiringScore+4;
							compose.Add('fzbutton', 4);
							end;
						if _getWireValue(TWireName.zzbutton) then begin
							wiringScore:=wiringScore+4;
							compose.Add('zzbutton1', 4);
							end;
						if _getWireValue(TWireName.gnd1) then begin
							wiringScore:=wiringScore+4;
							compose.Add('gnd1', 4);
							end;
						end;
					end;
				end;
			end;
		{$ENDREGION}
	end;
	3: begin
		wiringScore:=_calcWireScore(scoringVersion3[site.device.devType][site.paper.wiring]);
		end;
	4, 5: begin
		wiringScore:=_calcWireScore(scoringVersion4[site.device.devType][site.paper.wiring]);
		end;
	end;

case site.device.devType of
	TDeviceType.A: begin
		if site.answer.onoff then begin
			if site.answer.shunxu then //开关全对
			else begin
				wiringScore:=wiringScore-20;
				compose.Add('开关, shunxu', -20);
				end
			end
		else begin
			wiringScore:=wiringScore-20;
			compose.Add('开关,onoff', -20);
			end;

		if not site.answer.switch2 then begin
			faultPointScore:=faultPointScore-10;
			compose.Add('开关', -10);
			end;
		end;

	TDeviceType.B: begin
		if site.answer.onoff then
			if site.answer.shunxu  then //开关全对
			else begin
				faultPointScore:=faultPointScore-10;
				compose.Add('开关', -10);
				end
		else begin
			faultPointScore:=faultPointScore-10;
			compose.Add('开关', -10);
			end;

		if not site.answer.switch2 then begin
			wiringScore:=wiringScore-20;
			compose.Add('开关,switch2', -20);
			end;
		end;
	end;

if wiringScore<0 then
	wiringScore:=0;
if faultPointScore<0 then
	faultPointScore:=0;

Result:=faultPointScore+wiringScore;

if (site.answer.fuse<>nil) and (site.answer.fuse.value) then begin
	compose.Add('保险丝', -Result);
	Result:=0;
	end;
end;

constructor CExamLV.Create;
begin
Randomize();

callInMainThread:=CCallInMainThread.create;

tcpServer:=TIdTCPServer.Create(nil);
tcpServer.OnExecute:=Self.TcpServerOnExecute;
tcpServer.OnConnect:=self.TcpServerOnConnect;
tcpServer.OnDisconnect:=Self.TcpServerOnDisconnect;

sites_:=CSiteList.Create;
end;

destructor CExamLV.Destroy;
begin
callInMainThread.Free;

tcpServer.OnDisconnect:=nil;
tcpServer.OnExecute:=nil;
tcpServer.helperForeachContexts(
	procedure(contexts: TList)
	var
		i: Integer;
	begin
	for i:=0 to contexts.Count-1 do begin
		TIdContext(contexts[i]).Data:=nil;
		end;
	contexts.Clear;
	end);
tcpServer.Active:=False;
tcpServer.Free;

sites_.Free;
  inherited;
end;

procedure CExamLV.exam(site: CPracticeSiteLV);
var
	ip: string;
	paper: TPaper;
begin
ip:=site.ip;
paper:=site.paper;

tcpServer.helperForeachContexts(
	procedure(contexts: TList)

		function _paperToProtocol(const paper: TPaper): string;
		var
			i: Integer;
			s: string;
		begin
		result:='';

		case site.device.devType of
			TDeviceType.A: result:='#';
			TDeviceType.B: result:='#';
			end;

		s:='';
		for i:=0 to High(paper.faultPoint) do
			if paper.faultPoint[i] then begin
				if s='' then
					s:=inttostr(i+1)
				else
					s:=s+'+'+inttostr(i+1);
				end;
		result:=result+s+'+0'; //原先的第三题不出了，改为0

		result:=result+'+0+';

		case paper.wiring of
			0: result:=result+'11';
			1: result:=result+'22';
			2: result:=result+'33';
			end;
		end;

	var
		c: Pointer;
	begin
	for c in contexts do begin
		if TIdContext(c).Connection.Socket.Binding.PeerIP=ip then begin
			TIdContext(c).Data:=CString.create(_paperToProtocol(paper));
			break;
			end;
		end;
	end);
end;

function CExamLV.randromPaper(devType: TDeviceType; info: string): TPaper;

//	function _randromPick(qty: Byte): TArray<Byte>;
//	var
//		list: TList<Byte>;
//		i: Integer;
//		index: Integer;
//	begin
//	SetLength(result, qty);
//	list:=TList<Byte>.Create;
//	for i:=1 to fault_point_count-1 do
//		list.Add(i);
//	for i:=0 to qty-1 do begin
//		//B板现在排故不发第一第二个故障点
//		if devType=TDeviceType.B then
//			index:=RandomRange(2, list.Count)
//		else
//			index:=random(list.Count);
//		Result[i]:=list[index];
//		list.Delete(index);
//		end;
//	list.Free;
//	end;

var
	num: TArray<Integer>;
	i: Byte;
begin
FillChar(result, SizeOf(result), 0);

//随机故障点题
if devType=TDeviceType.A then
	if info='镇江' then
		num:=CRandom.pick<Integer>([0, 1, 3, 5, 6, 7], 2)// _randromPick(2);
	else if info='无锡' then
		num:=CRandom.pick<Integer>([0, 2, 3, 4, 5, 6, 7], 2)
	else
		num:=CRandom.pick<Integer>([0, 1, 2, 3, 4, 5, 6, 7], 2)// _randromPick(2);
else begin
	if version_>=3 then
		num:=CRandom.pick<Integer>([2, 4, 5, 6, 7], 2)
	else
		num:=CRandom.pick<Integer>([2, 3, 4, 5, 6, 7], 2);
	if info='无锡' then
		num:=CRandom.pick<Integer>([2, 4, 5, 6, 7], 2);
	end;

//根据单位名来屏蔽
if (info='溧阳市安全生产宣传教育中心低压电工实操考场') or (info='丰县现代安全技术培训中心低压实操考场') then begin
	if devType=TDeviceType.A then
		num:=CRandom.pick<Integer>([0, 2, 3, 4, 5, 6, 7], 2)// _randromPick(2);
	else
		num:=CRandom.pick<Integer>([2, 5, 6, 7], 2)
	end
else if (info='镇江市考试考核中心低压电工实操考场') then
	num:=CRandom.pick<Integer>([0, 1, 2, 4, 5, 6, 7], 2);// _randromPick(2);

for i in num do begin
	result.faultPoint[i]:=True;
	end;
//每个故障点的分值
for i:=0 to High(result.faultPoint) do
	if result.faultPoint[i] then
		result.faultPointScore[i]:=15
	else
		result.faultPointScore[i]:=-5;

//随机接线题（B板的接线第三题不要出现。）
if devType=TDeviceType.B then
	result.wiring:=Random(2)
else
	result.wiring:=Random(3);
//result.wiringScore:=40;
end;

procedure CExamLV.roll(site: CPracticeSiteLV);
var
	contexts: TList;
	i: Integer;
	s: string;
begin
s:=CCode.EitherOr<string>(site.device.devType=TDeviceType.A, '#', '#')+ 'over';
contexts:=tcpServer.Contexts.LockList;
try
	for i:=0 to contexts.Count-1 do begin
		if TIdContext(contexts[i]).Connection.Socket.Binding.PeerIP=site.ip then begin
			TIdContext(contexts[i]).Data:=CString.create(s);
			Break;
			end;
		end;
finally
	tcpServer.Contexts.UnlockList;
	end;
end;

procedure CExamLV.setVersion(value: Integer);
begin
Self.version_:=value;
case value of
	1: begin
		CExamLV.FPDefA:=FPDefA_CASE1;
		CExamLV.FPDefB:=FPDefB_CASE1;
		end;
	2: begin
		CExamLV.FPDefA:=FPDefA_CASE2;
		CExamLV.FPDefB:=FPDefB_CASE2;
		end;
	3,4: begin
		CExamLV.FPDefA:=FPDefA_CASE3;
		CExamLV.FPDefB:=FPDefB_CASE3;
		end;
	5: begin
		CExamLV.FPDefA:=FPDefA_CASE5;
		CExamLV.FPDefB:=FPDefB_CASE5;
		end;
	else
		raise Exception.Create('未知的版本号, '+value.ToString);
	end;
CDebug.WriteRecord('指定低压考试设备协议版本号, '+value.ToString, 'lv');
end;

{procedure CPracticeExam.test(dev: CDevice; content: string);
var
	contexts: TList;
	i: Integer;
begin
Assert(dev<>nil);

contexts:=tcpServer.Contexts.LockList;
try
	for i:=0 to contexts.Count-1 do begin
		if TIdContext(contexts[i]).Connection.Socket.Binding.PeerIP=dev.ip then begin
			TIdContext(contexts[i]).Data:=CString.create(content);
			break;
			end;
		end;
finally
	tcpServer.Contexts.UnlockList;
	end;
end;}

procedure CExamLV.open(port: Word);
begin
tcpServer.DefaultPort:=port;
tcpServer.Active:=True;
end;

procedure CExamLV.parse(request: string; site: CPracticeSiteLV);

	procedure _faultPoint(orderNr: Integer; p1, p2: Integer; isRight: Boolean);

		function _caseFaultPoint(): Byte;
		var
			byteset: TByteSet;
			i: Integer;
		begin
		Result:=255; //预置为错误码。
		case site.device.devType of
			TDeviceType.A: begin
				for i:=0 to fault_point_count-1 do
					if (p1 in FPDefA[i]) and (p2 in FPDefA[i]) then
						Exit(i);
				end;
			TDeviceType.B: begin
				for i:=0 to fault_point_count-1 do
					if (p1 in FPDefB[i]) and (p2 in FPDefB[i]) then
						Exit(i);
				end;
			end;
		end;

	var
		fp: Byte; //故障点
	begin
	fp:=_caseFaultPoint();
	if fp=255 then begin
		CDebug.WriteRecord('故障点分析失败, '+ request, 'lv', cdebug.TEventType.error);
		Exit;
		end;
//	if site.paper.faultPoint[fp] then //
//		site.answer.faultPoint[fp]:=isRight;
//	else
		site.answer.faultPoint[fp]:=True;

	CDebug.WriteRecord(Format('排故接线	工位：%d	接线点：%d-%d 题号：%d	%s', [orderNr, p1, p2, fp, booltostr(isRight)]), 'lv');
	end;

	procedure _switch(name: string; value: Integer);
	begin
	CDebug.WriteRecord(Format('开关状态, 开关名称：%s,  值: %s', [name, value.tostring]), 'lv');

	if name='onoff' then
		site.answer.onoff:=value=0
	else if name='shunxu' then
		site.answer.shunxu:=value=1
	else if name='switch2' then
		site.answer.switch2:=value=1
	else
		CDebug.WriteRecord('未知的开关名:'+name, 'lv', cdebug.TEventType.error);
	end;

	procedure _fuse(value: Integer);
	begin
	CDebug.WriteRecord(Format('保险丝状态: %s', [value.tostring]), 'lv');

	site.answer.fuse:=CBool.create(value=1);
	end;

	procedure _wire(name: string; isRight: Boolean);
	var
		i: Integer;
		wireName: TWireName;
		wire: TPair<TWireName, Boolean>;
	begin
	if name='Fuse' then //对保险丝来说，对错判断是反的
		isRight:=not isRight;

	CDebug.WriteRecord(Format('接线题	内容：%s	 对错：%s', [name, booltostr(isRight)]), 'lv');

	wireName:=TWireName(GetEnumvalue(TypeInfo(TWireName), name));

	for i:=0 to High(site.answer.wiring) do
		if site.answer.wiring[i].Key=wireName then begin
			site.answer.wiring[i].Value:=isRight;
			Exit;
			end;

	wire.Key:=wireName;
	wire.Value:=isRight;
	CArray.add<TPair<TWireName, Boolean>>(site.answer.wiring, wire);
	end;

	procedure _over(orderNr: Integer);
	begin
	CDebug.WriteRecord(Format('交卷	工位：%d', [orderNr]), 'lv');
	if Assigned(fOnSubmitPaper) then
		fOnSubmitPaper(site);
//	FillChar(site.paper, SizeOf(site.paper), 0);
	FillChar(site.answer, SizeOf(site.answer), 0);
	end;

	procedure _NotReset(orderNr: Integer);
	begin
	CDebug.WriteRecord(Format('未复位	工位：%d', [orderNr]), 'lv');
	if Assigned(fOnNotReset) then
		fOnNotReset(site);
	end;

	procedure _jiaojuan();
	var
		i: Integer;
	begin
	for i:=0 to High(site.answer.faultPoint) do
		site.answer.faultPoint[i]:=False;
	end;

var
	m: TMatch;
   s: string;
begin
{
@11+12-15+1		[@,#]工位+[wire1]-[wire2]+[0,1]     [@, #](\d+)\+(\d+)\-(\d+)\+(\d+)      排故
@11+switch1+0	[@,#]工位+switch[1,2]+[0,1]      	[@, #](\d+)\+switch([1,2])\+([0,1])   开关
@11+11+0			[@,#]工位+[11,22,33]+[0,1]          [@, #](\d+)\+(11|22|33)\+([0,1])    接线
@11+over                                           [@, #](\d+)\+over							  交卷
}
m:=TRegEx.Match(request, '[@, #](\d+)Unreset');
if m.Success then begin
	_NotReset(StrToInt(m.Groups[1].Value));
	end;

if Pos('jiaojuan', request)>0 then begin
	_jiaojuan();
	end;

m:=TRegEx.Match(request, '([@, #])(\d+)\+(\d+)\-(\d+)\+(\d+)');
if m.Success then begin
	while m.Success do begin
		_faultPoint(
			StrToInt(m.Groups[2].Value),  //工号
			StrToInt(m.Groups[3].Value),	StrToInt(m.Groups[4].Value), //接线
			StrToInt(m.Groups[5].Value)=1); //对错
		m:=m.NextMatch();
		end;
//	Exit;
	end;
m:=TRegEx.Match(request, '(onoff|shunxu|switch2)\+([0,1])');
if m.Success then begin
	while m.Success do begin
		_switch(m.Groups[1].Value, StrToInt(m.Groups[2].Value));
		m:=m.NextMatch();
		end;
//	Exit;
	end;

m:=TRegEx.Match(request, 'Fuse\+([0,1])');
if m.Success then begin
	while m.Success do begin
		_fuse(StrToInt(m.Groups[1].Value));
		m:=m.NextMatch();
		end;
//	Exit;
	end;

case Self.version_ of
	1: m:=TRegEx.Match(request, '(stopbutton|Zhengled|KM1|KM2|fanled|dengpao|button1|button2|Zhenliuqi|dankai1|dankai2|updn|Zlqbutton)\+(\d)');
	2: m:=TRegEx.Match(request, '(stopbutton|Zhengled|KM1|KM2|fanled|dengpao|button1|button2|Zhenliuqi|dankai1|dankai2|updn|Zlqbutton|slot1|slot2|gnd1|gnd2|zzbutton|fzbutton)\+(\d)');
	3: m:=TRegEx.Match(request, '(stopbutton|Zhengled|KM1|KM2|fanled|dengpao|button1|button2|Zhenliuqi|dankai1|dankai2|updn1|updn2|Zlqbutton|slot1|slot2|gnd1|gnd2|zzbutton|fzbutton)\+(\d)');
	4,5: m:=TRegEx.Match(request, '(stopbutton|Zhengled|KM1|KM2|fanled|dengpao|button1|button2|Zhenliuqi|dankai1|dankai2|updn1|updn2|Zlqbutton|slot1|slot2|gnd1|gnd2|zzbutton|fzbutton|EEmeter|CTransformer|Fuse)\+(\d)');
	end;
if m.Success then begin
	while m.Success do begin
		_wire(m.Groups[1].Value, StrToInt(m.Groups[2].Value)=1);
		m:=m.NextMatch();
		end;
	end;

m:=TRegEx.Match(request, '[@, #](\d+)\+over');
if m.Success then begin
	_over(StrToInt(m.Groups[1].Value));
	end;

//CLog.v('格式未知的数据：'+request, '');
end;

procedure CExamLV.TcpServerOnConnect(AContext: TIdContext);
type
  TCP_KeepAlive = record
	 OnOff: Cardinal;
	 KeepAliveTime: Cardinal;
	 KeepAliveInterval: Cardinal
  end;
var
  Val: TCP_KeepAlive;
  Ret: DWord;
var
	ip: string;
begin
Val.OnOff:=1;
Val.KeepAliveTime:=20*1000;
Val.KeepAliveInterval:=10*1000;
WSAIoctl(AContext.Connection.Socket.Binding.Handle, IOC_IN or IOC_VENDOR or 4, @Val, SizeOf(Val), nil, 0, Ret, nil, nil);

ip:=AContext.Connection.Socket.Binding.PeerIP;
callInMainThread.add(
	procedure()

		function _getType(): TDeviceType;
		var
			s: string;
		begin
		s:=cstring.extractStr(ip, '.', 3);
		if (StrToInt(s) mod 2)=0 then
			result:=TDeviceType.B
		else
			result:=TDeviceType.A;
		end;

		function _getOrderNr(): Byte;
		var
			s: string;
		begin
		s:=cstring.extractStr(ip, '.', 3);
		result:=StrToInt(s) mod 100;
		end;

	var
		site: CPracticeSiteLV;
	begin
	site:=CPracticeSiteLV.Create;
	site.ip:=ip;
	site.device.orderNr:=_getOrderNr();
	site.device.devType:=_getType();
	sites_.Add(site);
	if Assigned(fOnSiteConnect) then
		fOnSiteConnect(site);
	end);
end;

procedure CExamLV.TcpServerOnDisconnect(AContext: TIdContext);
var
	fromIp: string;
begin
fromIp:=AContext.Connection.Socket.Binding.PeerIP;
callInMainThread.add(procedure()
	var
		i: Integer;
		site: CPracticeSiteLV;
	begin
	for i:=0 to sites_.Count-1 do begin
		site:=sites_[i];
		if site.ip=fromIp then begin
			if Assigned(fOnSiteDisconnect) then
				fOnSiteDisconnect(site);
			sites_.Delete(i);
			break;
			end;
		end;
	end);
end;

procedure CExamLV.TcpServerOnExecute(AContext: TIdContext);
var
	s: UnicodeString;
	bs: TBytes;
	ip: string;
	i: Integer;
begin
Sleep(1);

if AContext.Connection.IOHandler.InputBuffer.Size>0 then begin
	ip:=AContext.Connection.Socket.Binding.PeerIP;
	try
		s:=AContext.Connection.IOHandler.ReadLn(#10);
	except
		on e: Exception do begin
			CDebug.WriteRecord('接收设备数据时出错，'+e.Message, 'lv', cdebug.TEventType.warning);
			Exit;
			end;
		end;
//	CDebug.WriteRecord('received '+s);
//	SetLength(bs, AContext.Connection.IOHandler.InputBuffer.Size);
//	for i:=0 to High(bs) do
//		bs[i]:=
//	s:=StringOf(bs);
//	.ReadLn(#10);
	callInMainThread.add(procedure()
		var
			site: CPracticeSiteLV;
		begin
		site:=sites_.IndexOfIp(ip);
		if site=nil then
			Exit;
		Self.parse(s, site);
		end);
	Exit;
	end;

if AContext.Data<>nil then begin
	s:=CString(AContext.Data).s;
	CString(AContext.Data).Free;
	AContext.Data:=nil;

	AContext.Connection.IOHandler.Write(s+#10);

//	CLog.v(s, 'send to '+AContext.Connection.Socket.Binding.PeerIp);
	end;
end;

{ CSiteList }

function CSiteList.indexOfIp(ip: string): CPracticeSiteLV;
var
	i: Integer;
begin
for i:=0 to Self.Count-1 do
	if Self[i].ip=ip then begin
		Exit(Self[i]);
		end;
Result:=nil;
end;

{ CPracticeSite }

procedure CPracticeSiteLV.clone(dest: CPracticeSiteLV);
begin
Assert(dest<>nil);
dest.ip:=Self.ip;
dest.port:=Self.port;
dest.device:=Self.device;
dest.paper:=Self.paper;
dest.answer:=Self.answer;
end;

{ TWireItemScore }

constructor TWireItemScore.create(wire: TWireName; score: Currency);
begin
self.wire:=wire;
self.score:=score;
end;

{ TPaper }

function TPaper.ToString: string;
var
	i: Integer;
begin
result:='faultPoint: ';
for i:=0 to High(self.faultPoint) do
	if self.faultPoint[i] then
		Result:=Result+i.ToString+' ';
Result:=Result+', wiring: '+wiring.ToString;
end;

end.
