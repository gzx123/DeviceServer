﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLCommon;
using Newtonsoft.Json.Linq;

namespace DeviceServer
{
    /// <summary>
    /// 低压设备终端
    /// </summary>
    public class LvSite:Site
    {
        private LowVoltage _lowVoltage;

        public LvSite()
        {
            //todo:AB板
            this.DeviceType = DeviceType.A;//   GetType(IP);
            _lowVoltage = new LowVoltage(this);
            _lowVoltage.DeviceStateAction = async (dstate) =>
            {
                await UpDeviceState(dstate);
            };
            _lowVoltage.SubmitAction = async (s) =>
            {
                await Submit(s,"k2");
            };
        }

     

        public const int FAULT_POINT_ALL_COUNT = 8;
        public const int FAULT_POINT_COUNT = 2;

        /// <summary>
        /// 试卷
        /// </summary>
        public Paper Paper { get; set; }
        /// <summary>
        /// 答案 
        /// </summary>
        public Answer Answer { get; set; }
        
        public DeviceType DeviceType { get; set; }

        public void TestCreatePaper(string cmd)
        {
            //cmd="#0+1+2+0+11";
            Paper paper = new Paper();
            paper.FaultPoint = new bool[FAULT_POINT_ALL_COUNT];
            paper.FaultPointScore = new decimal[FAULT_POINT_ALL_COUNT];
            List<int> fpindexs = new List<int>();
            fpindexs.Add((int.Parse(cmd[3].ToString()) - 1));
            fpindexs.Add((int.Parse(cmd[5].ToString()) - 1));
            for (int i = 0; i < paper.FaultPoint.Length; i++)
            {
                if (fpindexs.Contains(i))
                {
                    paper.FaultPoint[i] = true;
                    paper.FaultPointScore[i] = 12.5M;//15
                }
                else
                {
                    paper.FaultPoint[i] = false;
                    paper.FaultPointScore[i] = -5;
                }
            }
            int number = 0;
            var cmdnr = int.Parse(cmd.Substring(9, 2));
            switch (cmdnr)
            {
                case 11:
                    number = 0;
                    break;
                case 22:
                    number = 1;
                    break;
                case 33:
                    number = 2;
                    break;

            }


            paper.Wiring = number;
            Paper = paper;

            Answer answer = new Answer();
            string[] names = Enum.GetNames(typeof(WireName));
            foreach (var item in names)
            {
                WireName wireName = (WireName)Enum.Parse(typeof(WireName), item);
                answer.Wiring.Add(wireName, false);
            }
            Answer = answer;
        }

        /// <summary>
        /// 随机出题
        /// </summary>
        public void CreatePaper()
        {
            Random random = new Random();
            Paper paper = new Paper();

            List<int> fpindexs = RandomPick(FAULT_POINT_COUNT, DeviceType);
            paper.FaultPoint = new bool[FAULT_POINT_ALL_COUNT];
            paper.FaultPointScore = new decimal[FAULT_POINT_ALL_COUNT];
            for (int i = 0; i < paper.FaultPoint.Length; i++)
            {
                if (fpindexs.Contains(i))
                {
                    paper.FaultPoint[i] = true;
                    paper.FaultPointScore[i] = 12.5M;//15
                }
                else
                {
                    paper.FaultPoint[i] = false;
                    paper.FaultPointScore[i] = -5;
                }
            }
            int number = 0;
            switch (DeviceType)
            {
                case DeviceType.A:
                    number = random.Next(3);
                    break;
                case DeviceType.B:
                    number = random.Next(2);//B 板不出第三题
                    break;
            }
            paper.Wiring = number;
            Paper = paper;

            Answer answer = new Answer();
            string[] names = Enum.GetNames(typeof(WireName));
            foreach (var item in names)
            {
                WireName wireName = (WireName)Enum.Parse(typeof(WireName), item);
                answer.Wiring.Add(wireName, false);
            }
            Answer = answer;
        }

        /// <summary>
        /// 根据科目出题
        /// </summary>
        /// <param name="km"></param>
        internal void CreatePaper(string km)
        {
            Paper = new Paper();
            //故障点随机
            List<int> fpindexs = RandomPick(FAULT_POINT_COUNT, DeviceType);
            Paper.FaultPoint = new bool[FAULT_POINT_ALL_COUNT];
            Paper.FaultPointScore = new decimal[FAULT_POINT_ALL_COUNT];
            for (int i = 0; i < Paper.FaultPoint.Length; i++)
            {
                if (fpindexs.Contains(i))
                {
                    Paper.FaultPoint[i] = true;
                    Paper.FaultPointScore[i] = 12.5M;//15
                }
                else
                {
                    Paper.FaultPoint[i] = false;
                    Paper.FaultPointScore[i] = -5;
                }
            }

            Answer answer = new Answer();
            string[] names = Enum.GetNames(typeof(WireName));
            foreach (var item in names)
            {
                WireName wireName = (WireName)Enum.Parse(typeof(WireName), item);
                answer.Wiring.Add(wireName, false);
            }
            Answer = answer;
            Random r = new Random();
            switch (km)
            {
                case "k21":
                    Paper.Wiring = 0;//B 1
                    break;
                case "k22":
                    Paper.Wiring = 2;//B 3
                    break;
                case "k23":
                    Paper.Wiring = r.Next(2);//A板123
                    break;
                case "k24":
                    Paper.Wiring = r.Next(2);//B板12+3
                    break;
            }
        }

        /// <summary>
        /// 随机点题 排故题
        /// </summary>
        /// <param name="qty"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private List<int> RandomPick(int qty, DeviceType type)
        {
            var result = new List<int>();
            var list = new List<int>();
            int index;
            for (int i = 0; i < FAULT_POINT_ALL_COUNT; i++)
            {
                list.Add(i);
            }
            Random random = new Random();
            for (int i = 0; i < qty; i++)
            {
                if (type == DeviceType.B)
                    index = random.Next(list.Count);//2,list.count
                else
                    index = random.Next(list.Count);

                result.Add(list[index]);
                list.Remove(index);
            }
            return result;
        }

        DeviceType GetType(string ip)
        {
            string s = ip.Split('.')[3];
            int value = Convert.ToInt32(s);
            DeviceType device;
            if (value % 2 == 0)
                device = DeviceType.B;
            else
                device = DeviceType.A;
            return device;
        }

        /// <summary>
        /// 出题
        /// </summary>
        /// <returns></returns>
        public override string GetPaper()
        {
            CreatePaper(km);
            string paper = _lowVoltage.PaperToProtocal();
            return paper;
        }

        /// <summary>
        /// 解析返回值
        /// </summary>
        /// <param name="content"></param>
        public override void Parse(string content,List<string> steps )
        {
             _lowVoltage.Parse(content);
        }

        /// <summary>
        /// 收卷
        /// </summary>
        public async override void Roll()
        {
            var senddata = Encoding.UTF8.GetBytes("#over");
            await Session.SendAsync(senddata);
        }




       
    }
}
