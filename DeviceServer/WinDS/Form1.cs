﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cowboy.Sockets;
using DeviceServer;
using GLCommon;

namespace WinDS
{
    public partial class Form1 : Form
    {
        private static TcpSocketSaeaServer server;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LogTool.Init();
            LogTool.UpServerLog("DS 启动", EventType.Information);
            SimpleMessageDispatcher s = new SimpleMessageDispatcher();
            var config = new TcpSocketSaeaServerConfiguration();
            server = new TcpSocketSaeaServer(20300, s, config);
            server.Listen();
            while (true)
            {
                string text = Console.ReadLine();
                if (text == "quit")
                    break;

            }
            LogTool.UpServerLog("DS 关闭", EventType.Information);
            server.Shutdown();
        }

        private  void button2_Click(object sender, EventArgs e)
        {
            var senddata = Encoding.UTF8.GetBytes("#0+1+2+0+11");
           // await site.Session.SendAsync(senddata);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            decimal v = 200;
            string range = textBox2.Text;
           // var r = IsInRange(v, range);

            MessageBox.Show(RandomValue().ToString());
        }

        private decimal RandomValue()
        {
            Random random =new Random();
           var r = random.Next(200000);
            return r;
        }

        private bool IsInRange(decimal value, string range)
        {
            //"(x,xxxx]"
            int index = range.IndexOf(",", StringComparison.Ordinal);
            string v1 = range.Substring(1, index - 1);
            string v2 = range.Substring(index+1, range.Length -(index+1)-1);
            decimal left;
            decimal right;
            var leftsuccess = decimal.TryParse(v1, out left);
            var rightsuccess = decimal.TryParse(v2, out right);
            if (leftsuccess && rightsuccess)
            {
                return value > left && value < right;
            }
            if (!leftsuccess)
            {
                return value < right;
            }
            if (!rightsuccess)
            {
                return value > left;
            }
            return false;
        }
    }
}
